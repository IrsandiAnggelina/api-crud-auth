package config

import (
	"database/sql"
	"fmt"
)

const (
	username string = "root"
	password string = "password"
	database string = "db_api-crud"
)

var dsn = fmt.Sprintf("%v:%v@%v", username, password, database)

func MySQL() (*sql.DB, error){
	db, err := sql.Open("mysql", dsn)

	if err != nil{
		return nil, err
	}

	return db, nil
}