package main

import (
	"api-crud-case/auth"
	"api-crud-case/functions"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()

	router.GET("/bangun-datar/:jenis-bangun-datar", functions.HitungBangunDatar)

	router.GET("/categories", functions.GetCategories)
	router.GET("/categories/:id/books", functions.GetBookByCategory)
	router.POST("/categories", auth.BasicAuth(functions.PostCategories))
	router.PUT("/categories/:id", auth.BasicAuth(functions.UpdateCategories))
	router.DELETE("/categories/:id", auth.BasicAuth(functions.DeleteCategories))

	router.GET("/books", functions.GetBooks)
	router.POST("/books", auth.BasicAuth(functions.PostBooks))
	router.PUT("/books/:id", auth.BasicAuth(functions.UpdateBooks))
	router.DELETE("/books/:id", auth.BasicAuth(functions.DeleteBooks))


	fmt.Println("Server running at Port 8080")
	log.Fatal(http.ListenAndServe("localhost:8080", router))
}