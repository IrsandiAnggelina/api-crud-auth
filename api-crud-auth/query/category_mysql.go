package query

import (
	"api-crud-case/config"
	"api-crud-case/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"
)

const (
	category_table = "categories"
)

func GetAllCategories(ctx context.Context) ([]models.Category, error) {
	var allCategory []models.Category
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := "SELECT * FROM " + category_table + " Order By created_at DESC"
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var category models.Category
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&category.ID,
			&category.Name,
			&createdAt,
			&updatedAt,
		); err != nil {
			return nil, err
		}

		category.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		category.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		allCategory = append(allCategory, category)
	}
	return allCategory, nil
}

func GetAllBooksByCategory(ctx context.Context, params map[string]string, idCategory string) ([]models.Book, error) {
	var allBook []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	additionalQuery := ""
	sort := ""

	if len(params["title"]) > 0 {
		additionalQuery += " AND title LIKE '%" + params["title"] + "%'"
	}

	if len(params["minYear"]) > 0 {
		additionalQuery += " AND release_year >=" + params["minYear"]
	}

	if len(params["maxYear"]) > 0 {
		additionalQuery += " AND release_year <=" + params["maxYear"]
	}

	if len(params["minPage"]) > 0 {
		additionalQuery += " AND total_page >=" + params["minPage"]
	}

	if len(params["maxPage"]) > 0 {
		additionalQuery += " AND total_page <=" + params["maxPage"]
	}

	if len(params["sortByTitle"]) > 0 {
		if strings.ToLower(params["sortByTitle"]) == "desc" || strings.ToLower(params["sortByTitle"]) == "asc" {
			sort += " Order By Title " + strings.ToUpper(params["sortByTitle"])
		} else {
			sort += " Order By created_at DESC"
		}
	} else {
		sort += " Order By created_at DESC"
	}

	queryText := "SELECT * FROM " + book_table + " WHERE category_id=" + idCategory + additionalQuery + sort
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var book models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&book.ID,
			&book.Title,
			&book.Description,
			&book.ImageUrl,
			&book.ReleaseYear,
			&book.Price,
			&book.TotalPage,
			&book.Thickness,
			&createdAt,
			&updatedAt,
			&book.CategoryId,
		); err != nil {
			return nil, err
		}

		book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		allBook = append(allBook, book)
	}
	return allBook, nil
}

func InsertCategory(ctx context.Context, category models.Category) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (name, created_at, updated_at) values('%v', NOW(), NOW())", category_table, category.Name)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func UpdateCategory(ctx context.Context, category models.Category, idCategory string) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set name='%v', updated_at=NOW() where id=%v", category_table,
		category.Name,
		idCategory,
	)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func DeleteCategory(ctx context.Context, idCategory string) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id=%v", category_table, idCategory)
	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()

	if check == 0 {
		return errors.New("category not found")
	}

	if err != nil {
		fmt.Println(err.Error())
	}
	return nil
}
