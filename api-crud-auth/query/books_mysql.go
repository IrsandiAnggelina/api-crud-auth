package query

import (
	"api-crud-case/config"
	"api-crud-case/models"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"strings"
	"time"
)

const (
	book_table     = "books"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAllBooks(ctx context.Context, params map[string]string) ([]models.Book, error) {
	var allBook []models.Book
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	additionalQuery := ""
	sort := ""

	if len(params["title"]) > 0 {
		if len(additionalQuery) > 0 {
			additionalQuery += " AND title LIKE '%" + params["title"] + "%'"
		} else {
			additionalQuery += " WHERE title LIKE '%" + params["title"] + "%'"
		}
	}

	if len(params["minYear"]) > 0 {
		if len(additionalQuery) > 0 {
			additionalQuery += " AND release_year >=" + params["minYear"]
		} else {
			additionalQuery += " WHERE release_year >=" + params["minYear"]
		}
	}

	if len(params["maxYear"]) > 0 {
		if len(additionalQuery) > 0 {
			additionalQuery += " AND release_year <=" + params["maxYear"]
		} else {
			additionalQuery += " WHERE release_year <=" + params["maxYear"]
		}
	}

	if len(params["minPage"]) > 0 {
		if len(additionalQuery) > 0 {
			additionalQuery += " AND total_page >=" + params["minPage"]
		} else {
			additionalQuery += " WHERE total_page >=" + params["minPage"]
		}
	}

	if len(params["maxPage"]) > 0 {
		if len(additionalQuery) > 0 {
			additionalQuery += " AND total_page <=" + params["maxPage"]
		} else {
			additionalQuery += " WHERE total_page <=" + params["maxPage"]
		}
	}

	if len(params["sortByTitle"]) > 0 {
		if strings.ToLower(params["sortByTitle"]) == "desc" || strings.ToLower(params["sortByTitle"]) == "asc" {
			sort += " Order By Title " + strings.ToUpper(params["sortByTitle"])
		} else {
			sort += " Order By created_at DESC"
		}
	} else {
		sort += " Order By created_at DESC"
	}

	queryText := "SELECT * FROM " + book_table + additionalQuery + sort
	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var book models.Book
		var createdAt, updatedAt string
		if err = rowQuery.Scan(&book.ID,
			&book.Title,
			&book.Description,
			&book.ImageUrl,
			&book.ReleaseYear,
			&book.Price,
			&book.TotalPage,
			&book.Thickness,
			&createdAt,
			&updatedAt,
			&book.CategoryId,
		); err != nil {
			return nil, err
		}

		book.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		book.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		allBook = append(allBook, book)
	}
	return allBook, nil
}

func InsertBook(ctx context.Context, book models.Book) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (title, description, image_url, release_year, price, total_page, thickness, category_id, created_at, updated_at) values('%v','%v', '%v', %v, '%v', %v, '%v', %v , NOW(), NOW())", book_table,
		book.Title,
		book.Description,
		book.ImageUrl,
		book.ReleaseYear,
		book.Price,
		book.TotalPage,
		book.Thickness,
		book.CategoryId,
	)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func UpdateBook(ctx context.Context, book models.Book, idBook string) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set title='%v', description='%v', image_url='%v', release_year=%v, price='%v', total_page=%v, thickness='%v', updated_at=NOW() where id=%v", book_table,
		book.Title,
		book.Description,
		book.ImageUrl,
		book.ReleaseYear,
		book.Price,
		book.TotalPage,
		book.Thickness,
		idBook,
	)
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil
}

func DeleteBook(ctx context.Context, idBook string) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cannot Connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id=%v", book_table, idBook)
	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()

	if check == 0 {
		return errors.New("book not found")
	}

	if err != nil {
		fmt.Println(err.Error())
	}
	return nil
}
