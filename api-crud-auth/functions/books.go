package functions

import (
	"api-crud-case/models"
	"api-crud-case/query"
	"api-crud-case/utils"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"github.com/julienschmidt/httprouter"
)

type BookInput struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	ImageUrl    string `json:"image_url"`
	ReleaseYear int    `json:"release_year"`
	Price       string `json:"price"`
	TotalPage   int    `json:"total_page"`
	CategoryId  int    `json:"category_id"`
}

func getThicknessCategory(total_page int) string {
	switch {
	case total_page <= 100:
		return "tipis"
	case total_page >= 101 && total_page <= 200:
		return "sedang"
	default:
		return "tebal"
	}
}

func GetBooks(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	title := r.URL.Query().Get("title")
	minYear := r.URL.Query().Get("minYear")
	maxYear := r.URL.Query().Get("maxYear")
	minPage := r.URL.Query().Get("minPage")
	maxPage := r.URL.Query().Get("maxPage")
	sortByTitle := r.URL.Query().Get("sortByTitle")

	params := map[string]string{
		"title":       title,
		"minYear":     minYear,
		"maxYear":     maxYear,
		"minPage":     minPage,
		"maxPage":     maxPage,
		"sortByTitle": sortByTitle,
	}

	books, err := query.GetAllBooks(ctx, params)
	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(rw, books, http.StatusOK)

}

func PostBooks(rw http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application/json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	var input BookInput
	var book models.Book

	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	// validation section
	validation := ""

	_, errURL := url.ParseRequestURI(input.ImageUrl)

	if errURL != nil {
		if len(validation) == 0 {
			validation = "input image url harus berupa url yang valid"
		} else {
			validation += " dan input image url harus berupa url yang valid"
		}
	}

	if input.ReleaseYear < 1980 || input.ReleaseYear > 2021 {
		if len(validation) == 0 {
			validation = "release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		} else {
			validation += " dan release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		}
	}

	if len(validation) > 0 {
		utils.ResponseJSON(rw, validation, http.StatusBadRequest)
		return
	}
	// validation section

	// insert book object section
	book.Title = input.Title
	book.Description = input.Description
	book.ImageUrl = input.ImageUrl
	book.ReleaseYear = input.ReleaseYear
	book.Price = input.Price
	book.TotalPage = input.TotalPage
	book.Thickness = getThicknessCategory(input.TotalPage)
	book.CategoryId = input.CategoryId

	if err := query.InsertBook(ctx, book); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

func UpdateBooks(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application/json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	var input BookInput
	var book models.Book
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	// validation section
	validation := ""

	_, errURL := url.ParseRequestURI(input.ImageUrl)

	if errURL != nil {
		if len(validation) == 0 {
			validation = "input image url harus berupa url yang valid"
		} else {
			validation += " dan input image url harus berupa url yang valid"
		}
	}

	if input.ReleaseYear < 1980 || input.ReleaseYear > 2021 {
		if len(validation) == 0 {
			validation = "release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		} else {
			validation += " dan release year hanya harus diisi dari rentang tahun 1980 s/d 2021"
		}
	}

	if len(validation) > 0 {
		utils.ResponseJSON(rw, validation, http.StatusBadRequest)
		return
	}
	// validation section

	// insert book object section
	book.Title = input.Title
	book.Description = input.Description
	book.ImageUrl = input.ImageUrl
	book.ReleaseYear = input.ReleaseYear
	book.Price = input.Price
	book.TotalPage = input.TotalPage
	book.Thickness = getThicknessCategory(input.TotalPage)
	book.CategoryId = input.CategoryId

	var idBook = ps.ByName("id")

	if err := query.UpdateBook(ctx, book, idBook); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

func DeleteBooks(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var idBook = ps.ByName("id")

	if err := query.DeleteBook(ctx, idBook); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}
