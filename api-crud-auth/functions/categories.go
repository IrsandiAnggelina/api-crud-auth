package functions

import (
	"api-crud-case/models"
	"api-crud-case/query"
	"api-crud-case/utils"
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func GetCategories(rw http.ResponseWriter, r *http.Request, _ httprouter.Params){
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	categories, err := query.GetAllCategories(ctx)
	if err != nil{
		fmt.Println(err)
	}
	utils.ResponseJSON(rw, categories, http.StatusOK)
}

func GetBookByCategory (rw http.ResponseWriter, r *http.Request, ps httprouter.Params){
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	title := r.URL.Query().Get("title")
	minYear := r.URL.Query().Get("minYear")
	maxYear := r.URL.Query().Get("maxYear")
	minPage := r.URL.Query().Get("minPage")
	maxPage := r.URL.Query().Get("maxPage")
	sortByTitle := r.URL.Query().Get("sortByTitle")

	params := map[string]string{
		"title":       title,
		"minYear":     minYear,
		"maxYear":     maxYear,
		"minPage":     minPage,
		"maxPage":     maxPage,
		"sortByTitle": sortByTitle,
	}

	var idCategory = ps.ByName("id")

	books, err := query.GetAllBooksByCategory(ctx, params, idCategory)
	if err != nil {
		fmt.Println(err)
	}

	utils.ResponseJSON(rw, books, http.StatusOK)
}

func PostCategories (rw http.ResponseWriter, r *http.Request, ps httprouter.Params){
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application/json", http.StatusBadRequest)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var category models.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	if err := query.InsertCategory(ctx, category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

func UpdateCategories(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(rw, "Gunakan content type application/json", http.StatusBadRequest)
		return
	}
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	var category models.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(rw, err, http.StatusBadRequest)
		return
	}

	var idCategory = ps.ByName("id")

	if err := query.UpdateCategory(ctx, category, idCategory); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}

func DeleteCategories(rw http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var idCategory = ps.ByName("id")

	if err := query.DeleteCategory(ctx, idCategory); err != nil {
		utils.ResponseJSON(rw, err, http.StatusInternalServerError)
		return
	}
	res := map[string]string{
		"status": "Succesfully",
	}
	utils.ResponseJSON(rw, res, http.StatusCreated)
}